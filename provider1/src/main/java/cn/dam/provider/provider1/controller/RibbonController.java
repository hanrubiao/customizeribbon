package cn.dam.provider.provider1.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @ClassName RibbonController
 * @Description TODO
 * @Author dam
 * @Date 2018/11/28 2:38 PM
 * Version 1.0
 **/
@RestController
public class RibbonController {

    @GetMapping(value = "/getMsg")
    public String getMsg(){
        return "hi i am provider1";
    }



}
