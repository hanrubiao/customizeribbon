package cn.dam.consumer.consumer1.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.loadbalancer.LoadBalancerClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

/**
 * @ClassName RibbonController
 * @Description TODO
 * @Author dam
 * @Date 2018/11/28 2:29 PM
 * Version 1.0
 **/
@RestController
public class RibbonController {

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private LoadBalancerClient loadBalancerClient;

    @GetMapping(value = "/getMsg")
    public String getMsg(){
        return this.restTemplate.getForObject("http://provider1/getMsg",String.class);
    }



}
